# README #

gem5 code repositories for SPIN Deadlock Recovery Scheme

### Design Description ###
Synchronized Progress in Interconnection Networks (SPIN) : A New Theory for Deadlock Freedom

Aniruddh Ramrakhyani, Paul Gratz, and Tushar Krishna

In Proc of 45th International Symposium on Computer Architecture (ISCA), Jun 2018

	Paper: http://synergy.ece.gatech.edu/wp-content/uploads/sites/332/2018/04/spin_isca2018.pdf


### How to run ###

* See gem5/README

### Developer ###

* Aniruddh Ramrakhyani (aniruddh.ramrakhyani@gmail.com)